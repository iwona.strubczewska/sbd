package com.company.project.repository;

import com.company.project.model.ProjectEntity;
import com.company.project.model.TaskEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectRepository extends AbstractRepository<ProjectEntity> {
}
