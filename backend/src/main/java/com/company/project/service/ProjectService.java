package com.company.project.service;


import com.company.project.model.ProjectEntity;
import com.company.project.model.TaskEntity;

import java.util.List;

public interface ProjectService extends AbstractService<ProjectEntity> {
    List<ProjectEntity> findProjectsByUser(Long userId);
    ProjectEntity findProjectWithTasks(Long projectId);
    List<TaskEntity> findTasksOfProject(Long projectId);
}
