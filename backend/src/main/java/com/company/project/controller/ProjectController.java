package com.company.project.controller;

import com.company.project.dto.ProjectDto;
import com.company.project.dto.TaskDto;
import com.company.project.mapper.AbstractMapper;
import com.company.project.model.ProjectEntity;
import com.company.project.model.TaskEntity;
import com.company.project.service.AbstractService;
import com.company.project.service.ProjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/projects")
@CrossOrigin(origins = "http://localhost:4200")
public class ProjectController extends AbstractController<ProjectEntity, ProjectDto> {
    private AbstractMapper<TaskEntity, TaskDto> taskMapper;
    private AbstractMapper<ProjectEntity,ProjectDto> projectMapper;

    @Autowired
    public ProjectController(AbstractMapper<ProjectEntity, ProjectDto> projectMapper,
                             AbstractService<ProjectEntity> abstractService,AbstractMapper<TaskEntity,TaskDto> taskMapper) {
        super(projectMapper, abstractService, LoggerFactory.getLogger(ProjectController.class));
        this.projectMapper = projectMapper;
        this.taskMapper = taskMapper;
    }

    @GetMapping("/withTasks/{projectId}")
    public List<TaskDto> getProjectWithAssignedTasks(@PathVariable("projectId") Long projectId) {
        ProjectEntity projectEntity = this.getProjectService().findProjectWithTasks(projectId);
        ProjectDto projectDto = this.getAbstractMapper().fromEntityToNewDto(projectEntity);
        Set<TaskEntity> taskEntities = projectEntity.getTasks();
        List<TaskDto> taskDtos = new LinkedList<>();
        for(TaskEntity taskEntity : taskEntities){
            TaskDto taskDto = taskMapper.fromEntityToNewDto(taskEntity);
            taskDtos.add(taskDto);
        }
        projectDto.setTasks(taskDtos);
        return taskDtos;
    }

    private ProjectService getProjectService() {
        return (ProjectService) this.getAbstractService();
    }

    @GetMapping("/ofUser/{userId}")
    public List<ProjectDto> findProjectsByUser(@PathVariable("userId")Long userId){
        List<ProjectEntity> projectsOfUser = getProjectService().findProjectsByUser(userId);
        List<ProjectDto> projectOfUserDto = new LinkedList<>();
        for(ProjectEntity projectEntity : projectsOfUser){
            ProjectDto projectDto = projectMapper.fromEntityToNewDto(projectEntity);
            projectDto.setTasks(findTasksOfProject(projectDto.getId()));
            projectOfUserDto.add(projectDto);
        }
        return projectOfUserDto;
    }

    @GetMapping("/tasks/{projectId}")
    public List<TaskDto> findTasksOfProject(@PathVariable("projectId") Long projectId){
        List<TaskEntity> taskEntities = getProjectService().findTasksOfProject(projectId);
        List<TaskDto> taskDtos = new LinkedList<>();
        for(TaskEntity taskEntity : taskEntities){
            taskDtos.add(taskMapper.fromEntityToNewDto(taskEntity));
        }
        return taskDtos;
    }
}
