package com.company.project.service_impl;


import com.company.project.model.GroupEntity;
import com.company.project.model.ProjectEntity;
import com.company.project.model.TaskEntity;
import com.company.project.model.UserEntity;
import com.company.project.repository.AbstractRepository;
import com.company.project.repository.ProjectRepository;
import com.company.project.repository.TaskRepository;
import com.company.project.service.GroupService;
import com.company.project.service.ProjectService;
import com.company.project.service.UserService;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
@Transactional
public class ProjectServiceImpl extends AbstractServiceImpl<ProjectEntity> implements ProjectService {
    @Autowired
    private UserService userService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private ProjectRepository projectRepository;

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    public ProjectServiceImpl(AbstractRepository<ProjectEntity> abstractRepository) {
        super(abstractRepository);
    }

    @Override
    public List<ProjectEntity> findProjectsByUser(Long userId){
        List<ProjectEntity> projectsOfUser = new LinkedList<>();
        UserEntity userEntity = userService.read(userId);
        GroupEntity groupEntity = userEntity.getGroup();
        groupEntity.getTables().forEach(table -> projectsOfUser.add(table.getProject()));
        return projectsOfUser;
    }

    @Override
    public ProjectEntity findProjectWithTasks(Long projectId) {
        ProjectEntity projectEntity = this.read(projectId);
        Hibernate.initialize(projectEntity.getTasks());
        return projectEntity;
    }
    @Override
    public List<TaskEntity> findTasksOfProject(Long projectId){
        return taskRepository.findTasksByProjectId(projectId);
    }
}
