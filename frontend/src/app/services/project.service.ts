import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharedService } from './shared.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

    httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Access-Control-Allow-Origin': '*'
      })
    };
    prefix = 'http://localhost:8080';
    projectsUrl = this.prefix + '/projects';
    projectsOfUserUrl = this.prefix + '/projects/ofUser/{userId}';
    tasksOfProject = this.prefix + '/projects/tasks/{projectId}';
    constructor(private http: HttpClient, private sharedService: SharedService) {}
    getProjectsOfLoggedUser(): Observable<any> {
      console.log('user id' , this.sharedService.userId);
      const url = this.projectsOfUserUrl.replace('{userId}', this.sharedService.userId);
      console.log('zwraca:' , this.http.get(url));
      return this.http.get(url);
    }

    getTasksOfProject(projectId: string): Observable<any> {
      console.log('project Id : ', projectId);
      const url = this.tasksOfProject.replace('{projectId}', projectId );
      return this.http.get(url);
    }
}
