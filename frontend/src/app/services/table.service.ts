import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharedService } from './shared.service';
@Injectable({
  providedIn: 'root'
})
export class TableService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };
  prefix = 'http://localhost:8080';
  tables = this.prefix + '/tables';
  constructor(private http: HttpClient, private sharedService: SharedService) {}

    getAllTables(): Observable<any> {
      return this.http.get(this.tables);
    }
}
