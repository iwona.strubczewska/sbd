import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SharedService } from './shared.service';

@Injectable({
  providedIn: 'root'
})
export class GroupService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin': '*'
    })
  };
  prefix = 'http://localhost:8080';
  groups = this.prefix + '/groups';
  constructor(private http: HttpClient, private sharedService: SharedService) {}


    getAllGroups(): Observable<any> {
      return this.http.get(this.groups);
    }
}
