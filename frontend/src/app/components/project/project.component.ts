import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../../services/project.service';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {

  constructor(private projectService: ProjectService) { }
  projectsOfLoggedUser: Array<any>;
  tasks: Array<any>;

  ngOnInit() {
    this.projectService.getProjectsOfLoggedUser().subscribe(res => {
      console.log('PROJECTS OF LOGGED USER:', res);
      this.projectsOfLoggedUser = res;
    });
  }

  getTasks(projectId: string) {
    return this.projectsOfLoggedUser.find(x => x.id === projectId).tasks;
  }
}
