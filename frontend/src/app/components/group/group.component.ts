import { Component, OnInit } from '@angular/core';
import { GroupService } from '../../services/group.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {

  constructor(private groupService: GroupService) { }

  allGroups: Array<any>;

  ngOnInit() {
    console.log('component');
    this.groupService.getAllGroups().subscribe(res => {
      this.allGroups = res;
    });
  }

}
